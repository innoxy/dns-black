0.0.0.0 farm-de.plista.com          # /Advertiser/plista GmbH/Germany/http://www.plista.com
0.0.0.0 static-de.plista.com        # /Advertiser/plista GmbH/Germany/http://www.plista.com
0.0.0.0 trc.taboola.com             # /Advertiser/Taboola/USA/http://www.taboola.com
0.0.0.0 app-measurement.com         #/Tracking/Google Inc./USA/https://firebase.google.com/products/analytics/
0.0.0.0 push5.adups.com             #/Spyware/ToDo/https://heise.de/-3486446
0.0.0.0 ap.adups.com
0.0.0.0 fota5.adups.com
0.0.0.0 ad.yieldlab.net
0.0.0.0 s236.meetrics.net           # |Advertiser|Meetrics GmbH Berlin|Germany|https://www.meetrics.com
0.0.0.0 de-gmtdmp.mookie1.com       # |Advertiser|Media Innovation Group|?|http://themig.com
0.0.0.0 liftoff.io                  # |Advertiser|Liftoff Mobile, Inc|USA, Palo Alto|https://liftoff.io

#
0.0.0.0 *.ioam.de

# Census / Tracking
0.0.0.0 heise.met.vgwort.de
0.0.0.0 ssl-heise.met.vgwort.de